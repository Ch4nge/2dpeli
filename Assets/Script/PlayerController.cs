﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	private Rigidbody2D body;
	public float speed;
	public float jumpForce;
	private Animator animator;
	public GameObject texture;

	public Transform groundCheckPos;
	private bool grounded;
	public LayerMask floorLayer;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		grounded = Physics2D.Linecast (transform.position, groundCheckPos.position, floorLayer);
	}

	void FixedUpdate() {
		move ();
		jump ();
	}

	public void move() {
		if (Input.GetKey (KeyCode.A)) {
			body.AddForce (Vector2.left * speed * Time.deltaTime);
			texture.GetComponent<SpriteRenderer> ().flipX = true;
			animator.SetBool ("isMoving", true);
		} else if (Input.GetKey (KeyCode.D)) {
			body.AddForce (Vector2.right * speed * Time.deltaTime);
			texture.GetComponent<SpriteRenderer> ().flipX = false;
			animator.SetBool ("isMoving", true);
		} else {
			animator.SetBool ("isMoving", false);
		}
	
	}

	public void jump(){
		if (Input.GetKeyDown (KeyCode.Space) && grounded) {
			body.AddForce (Vector2.up * jumpForce * Time.deltaTime, ForceMode2D.Impulse);
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "Finish") {
			SceneManager.LoadScene ("LevelSelect");
		}
	}
}
